/* eslint-disable react/jsx-filename-extension */
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import rootReducer from '../src/reducers';

const defaultState = {};

const createState = (initialState) => (actions) =>
  actions.reduce(rootReducer, initialState);

// eslint-disable-next-line import/prefer-default-export
export const getMockStore = (mockState) => {
  const initialState = createState(mockState || defaultState);
  const middlewares = [thunk];
  const store = configureMockStore(middlewares)(initialState);
  store.replaceReducer(rootReducer);
  return store;
};
