import axios from 'axios';
import pick from 'lodash/pick';

const BASE_URL = 'http://localhost:5000/api';
const UserFields = ['employeeId', 'username', 'name', 'salary'];

export default {
  listUsers: async ({
    limit,
    query,
    page,
    order,
    orderBy,
    maxSalary,
    minSalary,
  }) => {
    const sort = (order === 'desc' ? '-' : '+') + orderBy;
    const { data } = await axios
      .get(`${BASE_URL}/users`, {
        params: {
          limit,
          offset: limit && page * limit,
          query,
          sort,
          maxSalary,
          minSalary,
        },
      })
      .catch((error) => {
        throw new Error(error.response.data.message || error.message);
      });
    return data;
  },

  createUser: async (userData) => {
    const { data } = await axios
      .post(`${BASE_URL}/users`, pick(userData, UserFields))
      .catch((error) => {
        throw new Error(error.response.data.message || error.message);
      });
    return data;
  },

  updateUser: async (userData) => {
    const { data } = await axios
      .post(`${BASE_URL}/users/${userData.id}`, pick(userData, UserFields))
      .catch((error) => {
        throw new Error(error.response.data.message || error.message);
      });
    return data;
  },

  deleteUser: async (id) => {
    const { data } = await axios
      .delete(`${BASE_URL}/users/${id}`)
      .catch((error) => {
        throw new Error(error.response.data.message || error.message);
      });
    return data;
  },
  uploadUsers: async (formData) => {
    const { data } = await axios
      .post(`${BASE_URL}/users/upload`, formData)
      .catch((error) => {
        throw new Error(error.response.data.message || error.message);
      });
    return data;
  },
};
