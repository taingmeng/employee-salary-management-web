export default (value = 0) =>
  `${value.toLocaleString('en-SG', {
    style: 'currency',
    currency: 'SGD',
  })}`;
