import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { alertSelector, alertAction } from '../../reducers/alerts';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ConsecutiveSnackbars = ({ messages, deleteMessage }) => {
  const [open, setOpen] = React.useState(false);
  React.useEffect(() => {
    if (messages.length) {
      setOpen(true);
    } else if (messages.length && open) {
      setOpen(false);
    }
  }, [messages]);

  const handleClose = () => {
    setOpen(false);
    deleteMessage(messages[0].id);
  };

  const handleExited = () => {
    setOpen(false);
    deleteMessage(messages[0].id);
  };

  return (
    <div>
      <Snackbar
        key={messages.length && messages[0].id}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        onExited={handleExited}
      >
        <Alert severity={messages.length ? messages[0].type : 'info'}>
          {messages.length ? messages[0].message : undefined}
        </Alert>
      </Snackbar>
    </div>
  );
};

ConsecutiveSnackbars.propTypes = {
  messages: PropTypes.array,
  deleteMessage: PropTypes.func,
};

const mapStateToProps = (state) => ({
  messages: alertSelector.getMessages(state),
});

const mapDispatchToProps = (dispatch) => ({
  deleteMessage: (id) => dispatch(alertAction.deleteMessage(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConsecutiveSnackbars);
