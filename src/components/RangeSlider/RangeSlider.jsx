import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import { Tooltip } from '@material-ui/core';

import { formatCurrency } from '../../helpers';

const ValueLabelComponent = ({ children, open, value }) => (
  <Tooltip
    open={open}
    enterTouchDelay={0}
    placement="bottom"
    title={formatCurrency(value)}
  >
    {children}
  </Tooltip>
);

ValueLabelComponent.propTypes = {
  children: PropTypes.any,
  open: PropTypes.bool,
  value: PropTypes.number,
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2),
  },
}));

const RangeSlider = ({ title, min, max, onChangeCommitted }) => {
  const classes = useStyles();

  const [value, setValue] = React.useState([min || 0, max || 100]);

  useEffect(() => {
    setValue([min || 0, max || 100]);
  }, [min, max]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Typography id="range-slider">{title}</Typography>
      <Slider
        max={max}
        min={min}
        step={10}
        aria-labelledby="discrete-slider"
        value={value}
        onChange={handleChange}
        ValueLabelComponent={ValueLabelComponent}
        valueLabelDisplay="on"
        onChangeCommitted={onChangeCommitted}
      />
    </div>
  );
};

RangeSlider.propTypes = {
  title: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  onChangeCommitted: PropTypes.func,
};

export default RangeSlider;
