import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { connect } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  AppBar,
  Toolbar,
  Button,
  Menu,
  MenuItem,
  Hidden,
  IconButton,
  Typography,
} from '@material-ui/core';
import { withTranslation } from 'react-i18next';

import MenuIcon from '@material-ui/icons/Menu';
import { settingSelector, settingAction } from '../../../../reducers/settings';
import { LanguageMenuItems } from '../../../../reducers/settings/reducer';

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: 'none',
  },
  title: {
    color: theme.palette.primary.contrastText,
  },
  flexGrow: {
    flexGrow: 1,
  },
  menu: {
    color: theme.palette.primary.contrastText,
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
  },
}));

const Topbar = ({
  className,
  onSidebarOpen,
  localeLabel,
  setLanguage,
  i18n,
  t,
  ...rest
}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    if (anchorEl === null) {
      setAnchorEl(event.currentTarget);
    }
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSelect = ({ value, label }) => {
    i18n.changeLanguage(value);
    setLanguage({ value, label });
    setAnchorEl(null);
  };

  return (
    <AppBar {...rest} className={clsx(classes.root, className)}>
      <Toolbar>
        <RouterLink to="/">
          <Typography variant="h3" className={classes.title}>
            {t('CMS')}
          </Typography>
        </RouterLink>
        <div className={classes.flexGrow} />
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          className={classes.menu}
        >
          {localeLabel}
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClick={handleClick}
          onClose={handleClose}
        >
          {LanguageMenuItems.map(({ value, label }) => (
            <MenuItem
              key={value}
              onClick={() => handleSelect({ value, label })}
            >
              {label}
            </MenuItem>
          ))}
        </Menu>
        <Hidden lgUp>
          <IconButton color="inherit" onClick={onSidebarOpen}>
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func,
  localeLabel: PropTypes.string,
  setLanguage: PropTypes.func,
  i18n: PropTypes.object,
  t: PropTypes.func,
};

const mapStateToProps = (state) => ({
  localeLabel: settingSelector.getLocaleLabel(state),
});

const mapDispatchToProps = (dispatch) => ({
  setLanguage: ({ value, label }) =>
    dispatch(settingAction.setLanguage({ value, label })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation()(Topbar));
