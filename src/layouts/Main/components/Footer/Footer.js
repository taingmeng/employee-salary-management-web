import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Typography } from '@material-ui/core';
import { withTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}));

const Footer = ({ className, t, ...rest }) => {
  const classes = useStyles();

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Typography variant="body1">
        &copy; {t('Employee Salary Management')} 2020
      </Typography>
    </div>
  );
};

Footer.propTypes = {
  className: PropTypes.string,
  t: PropTypes.func.isRequired,
};

export default withTranslation()(Footer);
