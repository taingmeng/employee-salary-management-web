import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';
import { withTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content',
  },
  avatar: {
    width: 60,
    height: 60,
  },
  name: {
    marginTop: theme.spacing(1),
  },
}));

const Profile = ({ className, t, ...rest }) => {
  const classes = useStyles();

  const user = {
    name: 'Meng Taing',
    avatar: '/images/avatars/avatar_1.jpg',
    bio: 'Human Resource Manager',
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Avatar
        alt="Person"
        className={classes.avatar}
        component={RouterLink}
        src={user.avatar}
        to="/account"
      />
      <Typography className={classes.name} variant="h4">
        {user.name}
      </Typography>
      <Typography variant="body2">{t(user.bio)}</Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string,
  t: PropTypes.func.isRequired,
};

export default withTranslation()(Profile);
