import get from 'lodash/get';

export const getLocale = (state) => get(state, 'settings.locale', 'en');
export const getLocaleLabel = (state) => get(state, 'settings.localeLabel');

export default {};
