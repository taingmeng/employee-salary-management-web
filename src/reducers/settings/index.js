import reducer from './reducer';
import * as settingSelector from './selector';
import * as settingAction from './action';

export { settingAction, settingSelector };

export default reducer;
