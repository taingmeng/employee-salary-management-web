export const SET_LANGUAGE = 'esm/settings/SET_LANGUAGE';

export function setLanguage({ value, label }) {
  return {
    type: SET_LANGUAGE,
    value,
    label,
  };
}
