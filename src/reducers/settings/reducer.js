import { SET_LANGUAGE } from './action';

export const LanguageMenuItems = [
  {
    value: 'en',
    label: 'English (EN)',
  },
  {
    value: 'zh',
    label: '中文 (ZH)',
  },
];

const reducer = (
  state = {
    locale: LanguageMenuItems[0].value,
    localeLabel: LanguageMenuItems[0].label,
  },
  action = {},
) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        locale: action.value,
        localeLabel: action.label,
      };
    default:
      return state;
  }
};

export default reducer;
