import { combineReducers } from 'redux';
import users from './users';
import alerts from './alerts';
import settings from './settings';

const rootReducer = combineReducers({
  alerts,
  users,
  settings,
});

export default rootReducer;
