import { ADD_MESSAGE, DELET_MESSAGE } from './action';

const reducer = (
  state = {
    messages: [],
  },
  action = {},
) => {
  switch (action.type) {
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [
          ...state.messages,
          {
            id: new Date().getTime(),
            message: action.message,
            type: 'error',
          },
        ],
      };

    case DELET_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter(({ id }) => id !== action.id),
      };
    default:
      return state;
  }
};

export default reducer;
