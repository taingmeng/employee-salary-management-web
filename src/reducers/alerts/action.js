export const ADD_MESSAGE = 'esm/alerts/ADD_MESSAGE';
export const DELET_MESSAGE = 'esm/alerts/DELET_MESSAGE';

export function addMessage(message) {
  return {
    type: ADD_MESSAGE,
    message,
  };
}

export function deleteMessage(id) {
  return {
    type: DELET_MESSAGE,
    id,
  };
}
