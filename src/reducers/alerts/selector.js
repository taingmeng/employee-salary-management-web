import get from 'lodash/get';

export const getMessages = (state) => get(state, 'alerts.messages', []);

export default {};
