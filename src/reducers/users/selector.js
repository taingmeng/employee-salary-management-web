import get from 'lodash/get';

export const getUsers = (state) => get(state, 'users.users', []);
export const getCount = (state) => get(state, 'users.count', 0);
export const getPage = (state) => get(state, 'users.page', 0);
export const getLimit = (state) => get(state, 'users.limit', 10);
export const getQuery = (state) => get(state, 'users.query');
export const getOrderBy = (state) => get(state, 'users.orderBy', 'employeeId');
export const getOrder = (state) => get(state, 'users.order', 'asc');
export const getMaxSalary = (state) => get(state, 'users.maxSalary');
export const getMinSalary = (state) => get(state, 'users.minSalary');
export const getQuerySalaryMax = (state) => get(state, 'users.querySalaryMax');
export const getQuerySalaryMin = (state) => get(state, 'users.querySalaryMin');
export const getUploading = (state) => get(state, 'users.uploading');

export default {};
