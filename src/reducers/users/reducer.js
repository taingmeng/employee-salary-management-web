import {
  SET_USERS,
  SET_PAGE,
  SET_LIMIT,
  SET_ORDER_BY,
  SET_ORDER,
  SET_QUERY,
  SET_COUNT,
  SET_MAX_SALARY,
  SET_MIN_SALARY,
  SET_QUERY_SALARY_RANGE,
  SET_UPLOADING,
} from './action';

const reducer = (
  state = {
    users: [],
    count: 0,
    query: null,
    limit: 10,
    page: 0,
    orderBy: 'employeeId',
    order: 'asc',
    maxSalary: 0,
    minSalary: 0,
    querySalaryMax: null,
    querySalaryMin: null,
    uploading: false,
  },
  action = {},
) => {
  switch (action.type) {
    case SET_USERS:
      return {
        ...state,
        users: action.users,
      };
    case SET_COUNT:
      return {
        ...state,
        count: action.count,
      };

    case SET_QUERY_SALARY_RANGE:
      return {
        ...state,
        querySalaryMin: action.range[0],
        querySalaryMax: action.range[1],
      };
    case SET_MIN_SALARY:
      return {
        ...state,
        minSalary: action.minSalary,
      };
    case SET_MAX_SALARY:
      return {
        ...state,
        maxSalary: action.maxSalary,
      };
    case SET_QUERY:
      return {
        ...state,
        query: action.query,
      };
    case SET_PAGE:
      return {
        ...state,
        page: action.page,
      };
    case SET_LIMIT:
      return {
        ...state,
        limit: action.limit,
      };
    case SET_ORDER_BY:
      return {
        ...state,
        orderBy: action.orderBy,
      };
    case SET_ORDER:
      return {
        ...state,
        order: action.order,
      };
    case SET_UPLOADING:
      return {
        ...state,
        uploading: action.uploading,
      };
    default:
      return state;
  }
};

export default reducer;
