import userApi from '../../api/userApi';
import { alertAction } from '../alerts';
import * as userSelector from './selector';

export const SET_USERS = 'esm/users/SET_USERS';
export const SET_COUNT = 'esm/COUNT/SET_USERS';
export const SET_PAGE = 'esm/users/SET_PAGE';
export const SET_LIMIT = 'esm/users/SET_LIMIT';
export const SET_ORDER_BY = 'esm/users/SET_ORDER_BY';
export const SET_ORDER = 'esm/users/SET_ORDER';
export const SET_QUERY = 'esm/users/SET_QUERY';
export const SET_QUERY_SALARY_RANGE = 'esm/users/SET_QUERY_SALARY_RANGE';
export const SET_MAX_SALARY = 'esm/users/SET_MAX_SALARY';
export const SET_MIN_SALARY = 'esm/users/SET_MIN_SALARY';
export const SET_UPLOADING = 'esm/users/SET_UPLOADING';

export function setUploading(uploading) {
  return {
    type: SET_UPLOADING,
    uploading,
  };
}

export function setCount(count) {
  return {
    type: SET_COUNT,
    count,
  };
}

export function setUsers(users) {
  return {
    type: SET_USERS,
    users,
  };
}

export function setMaxSalary(maxSalary) {
  return {
    type: SET_MAX_SALARY,
    maxSalary,
  };
}

export function setMinSalary(minSalary) {
  return {
    type: SET_MIN_SALARY,
    minSalary,
  };
}

export function listUsers(overrideParams = {}) {
  return async (dispatch, getState) => {
    const state = getState();
    const queryParams = {
      query: userSelector.getQuery(state),
      limit: userSelector.getLimit(state),
      page: userSelector.getPage(state),
      order: userSelector.getOrder(state),
      orderBy: userSelector.getOrderBy(state),
      maxSalary: userSelector.getQuerySalaryMax(state),
      minSalary: userSelector.getQuerySalaryMin(state),
    };
    try {
      const { rows, count, meta } = await userApi.listUsers({
        ...queryParams,
        ...overrideParams,
      });
      dispatch(setUsers(rows));
      dispatch(setCount(count));
      dispatch(setMaxSalary(meta.maxSalary));
      dispatch(setMinSalary(meta.minSalary));
    } catch ({ message }) {
      dispatch(alertAction.addMessage(message));
    }
  };
}

export function setQuery(query) {
  return (dispatch) => {
    dispatch({
      type: SET_QUERY,
      query,
    });
    dispatch(listUsers({ query }));
  };
}

export function setQuerySalaryRange([min, max]) {
  return (dispatch) => {
    dispatch({
      type: SET_QUERY_SALARY_RANGE,
      range: [min, max],
    });
    dispatch(listUsers({ querySalaryMax: max, querySalaryMin: min }));
  };
}

export function setPage(page) {
  return (dispatch) => {
    dispatch({
      type: SET_PAGE,
      page,
    });
    dispatch(listUsers({ page }));
  };
}

export function setLimit(limit) {
  return (dispatch) => {
    dispatch({
      type: SET_LIMIT,
      limit,
    });
    dispatch(listUsers({ limit }));
  };
}

export function setOrderBy(orderBy) {
  return (dispatch) => {
    dispatch({
      type: SET_ORDER_BY,
      orderBy,
    });
    dispatch(listUsers({ orderBy }));
  };
}

export function setOrder(order) {
  return {
    type: SET_ORDER,
    order,
  };
}

export function createUser(userData) {
  return async (dispatch) => {
    try {
      await userApi.createUser(userData);
      dispatch(listUsers());
    } catch ({ message }) {
      dispatch(alertAction.addMessage(message));
    }
  };
}

export function updateUser(userData) {
  return async (dispatch) => {
    try {
      await userApi.updateUser(userData);
      dispatch(listUsers());
    } catch ({ message }) {
      dispatch(alertAction.addMessage(message));
    }
  };
}

export function deleteUser(id) {
  return async (dispatch) => {
    try {
      await userApi.deleteUser(id);
      dispatch(listUsers());
    } catch ({ message }) {
      dispatch(alertAction.addMessage(message));
    }
  };
}

export function uploadUsers(file, { delay } = {}) {
  return async (dispatch) => {
    dispatch(setUploading(true));
    try {
      const formData = new FormData();
      formData.append('file', file, file.name);
      if (delay) {
        formData.append('delay', delay);
      }
      await userApi.uploadUsers(formData);
      dispatch(listUsers());
    } catch ({ message }) {
      dispatch(alertAction.addMessage(message));
    }
    dispatch(setUploading(false));
  };
}
