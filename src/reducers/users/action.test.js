import { userAction } from '.';
import { getMockStore } from '../../../tests/common';
import userApi from '../../api/userApi';

jest.mock('../../api/userApi');

describe('usersAtion', () => {
  describe('listUsers', () => {
    it('should call listUser api and dispatch result', async () => {
      const store = getMockStore();
      userApi.listUsers.mockResolvedValueOnce({
        count: 123,
        rows: [{ id: 1, employeeId: 'e001' }],
        meta: {
          minSalary: 1111.11,
          maxSalary: 9999.99,
        },
      });
      await store.dispatch(userAction.listUsers());

      expect(store.getState().users).toEqual({
        count: 123,
        limit: 10,
        maxSalary: 9999.99,
        minSalary: 1111.11,
        order: 'asc',
        orderBy: 'employeeId',
        page: 0,
        query: null,
        querySalaryMax: null,
        querySalaryMin: null,
        uploading: false,
        users: [{ employeeId: 'e001', id: 1 }],
      });
    });
  });
});
