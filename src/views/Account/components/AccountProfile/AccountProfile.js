import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Avatar, Typography } from '@material-ui/core';
import { withTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {},
  details: {
    display: 'flex',
  },
  avatar: {
    marginLeft: 'auto',
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0,
  },
  progress: {
    marginTop: theme.spacing(2),
  },
  uploadButton: {
    marginRight: theme.spacing(2),
  },
}));

const AccountProfile = ({ className, t, ...rest }) => {
  const classes = useStyles();

  const user = {
    name: 'Meng Taing',
    city: 'Singapore',
    country: 'Singapore',
    timezone: 'GTM+8',
    avatar: '/images/avatars/avatar_1.jpg',
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <div className={classes.details}>
          <div>
            <Typography gutterBottom variant="h2">
              {user.name}
            </Typography>
            <Typography
              className={classes.locationText}
              color="textSecondary"
              variant="body1"
            >
              {t(user.city)}, {t(user.country)}
            </Typography>
            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1"
            >
              {moment().format('hh:mm A')} ({user.timezone})
            </Typography>
          </div>
          <Avatar className={classes.avatar} src={user.avatar} />
        </div>
      </CardContent>
    </Card>
  );
};

AccountProfile.propTypes = {
  t: PropTypes.func.isRequired,
};

AccountProfile.propTypes = {
  className: PropTypes.string,
};

export default withTranslation()(AccountProfile);
