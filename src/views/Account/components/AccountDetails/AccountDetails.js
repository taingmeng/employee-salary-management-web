import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField,
} from '@material-ui/core';
import { withTranslation } from 'react-i18next';

const AccountDetails = ({ className, user, title, onSave, t, ...rest }) => {
  const [values, setValues] = useState(user || {});

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onSaveClick = () => {
    onSave(values);
  };

  return (
    <Card {...rest} className={clsx(className)}>
      <form autoComplete="off" noValidate>
        <CardHeader title={title} />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label={t('Employee ID')}
                margin="dense"
                name="employeeId"
                onChange={handleChange}
                required
                value={values.employeeId}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label={t('Username')}
                margin="dense"
                name="username"
                onChange={handleChange}
                required
                value={values.username}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label={t('Name')}
                margin="dense"
                name="name"
                onChange={handleChange}
                required
                value={values.name}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label={t('Salary')}
                margin="dense"
                name="salary"
                onChange={handleChange}
                required
                type="number"
                value={values.salary}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button color="primary" variant="contained" onClick={onSaveClick}>
            {t('Save Details')}
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

AccountDetails.propTypes = {
  className: PropTypes.string,
  user: PropTypes.object,
  title: PropTypes.string,
  onSave: PropTypes.func,
  t: PropTypes.func,
};

export default withTranslation()(AccountDetails);
