import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  TablePagination,
  IconButton,
  TableSortLabel,
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';

import { getInitials, formatCurrency } from '../../../../helpers';
import { userSelector, userAction } from '../../../../reducers/users';
import UserDetailsModal from '../UserDetailsModal';

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 1050,
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    marginRight: theme.spacing(2),
  },
  actions: {
    justifyContent: 'flex-end',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

const headCells = [
  {
    id: 'employeeId',
    numeric: false,
    disablePadding: false,
    label: 'Employee ID',
  },
  { id: 'name', numeric: false, disablePadding: false, label: 'Name' },
  {
    id: 'username',
    numeric: false,
    disablePadding: false,
    label: 'Username',
  },
  { id: 'salary', numeric: true, disablePadding: false, label: 'Salary' },
  {
    id: 'createdAt',
    numeric: false,
    disablePadding: false,
    label: 'Created at',
  },
];

const UsersTable = ({
  className,
  users,
  count,
  updateUser,
  deleteUser,
  order,
  setOrder,
  orderBy,
  setOrderBy,
  page,
  setPage,
  rowsPerPage,
  setRowsPerPage,
  t,
  ...rest
}) => {
  const classes = useStyles();
  const [userToBeEdited, setUserToBeEdited] = useState(null);
  const [editModalOpen, setEditModalOpen] = useState(false);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const createSortHandler = (property) => (event) => {
    handleRequestSort(event, property);
  };

  const handlePageChange = (event, currentPage) => {
    setPage(currentPage);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(event.target.value);
  };
  const handleDeleteUser = (userId) => {
    deleteUser(userId);
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={headCell.numeric ? 'right' : 'left'}
                      padding={headCell.disablePadding ? 'none' : 'default'}
                      sortDirection={orderBy === headCell.id ? order : false}
                    >
                      <TableSortLabel
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : 'asc'}
                        onClick={createSortHandler(headCell.id)}
                      >
                        {t(headCell.label)}
                        {orderBy === headCell.id ? (
                          <span className={classes.visuallyHidden}>
                            {order === 'desc'
                              ? 'sorted descending'
                              : 'sorted ascending'}
                          </span>
                        ) : null}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                  <TableCell>{t('Actions')}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {users.map((user) => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={user.id}
                  >
                    <TableCell>{user.employeeId}</TableCell>
                    <TableCell>
                      <div className={classes.nameContainer}>
                        <Avatar className={classes.avatar} src={user.avatarUrl}>
                          {getInitials(user.name)}
                        </Avatar>
                        <Typography variant="body1">{user.name}</Typography>
                      </div>
                    </TableCell>

                    <TableCell>{user.username}</TableCell>
                    <TableCell align="right">
                      {formatCurrency(user.salary)}
                    </TableCell>
                    <TableCell>
                      {moment(user.createdAt).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell>
                      <IconButton
                        aria-label="edit"
                        onClick={() => {
                          setUserToBeEdited(user);
                          setEditModalOpen(true);
                        }}
                      >
                        <EditIcon className={classes.statsIcon} />
                      </IconButton>
                      <IconButton
                        aria-label="delete"
                        onClick={() => handleDeleteUser(user.id)}
                      >
                        <DeleteIcon className={classes.statsIcon} />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
      <CardActions className={classes.actions}>
        <TablePagination
          component="div"
          count={count}
          onChangePage={handlePageChange}
          onChangeRowsPerPage={handleRowsPerPageChange}
          page={page}
          labelRowsPerPage={t('Rows per page')}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5, 10, 25]}
        />
      </CardActions>

      <UserDetailsModal
        title="Edit User"
        onSave={(userData) => {
          setEditModalOpen(false);
          updateUser(userData);
        }}
        handleClose={() => {
          setEditModalOpen(false);
        }}
        open={editModalOpen}
        user={userToBeEdited}
      />
    </Card>
  );
};

UsersTable.propTypes = {
  className: PropTypes.string,
  users: PropTypes.array.isRequired,
  count: PropTypes.string,
  updateUser: PropTypes.func,
  deleteUser: PropTypes.func,
  order: PropTypes.string,
  setOrder: PropTypes.func,
  orderBy: PropTypes.string,
  setOrderBy: PropTypes.func,
  page: PropTypes.number,
  setPage: PropTypes.func,
  rowsPerPage: PropTypes.number,
  setRowsPerPage: PropTypes.func,
  t: PropTypes.func,
};

const mapStateToProps = (state) => ({
  users: userSelector.getUsers(state),
  order: userSelector.getOrder(state),
  orderBy: userSelector.getOrderBy(state),
  rowsPerPage: userSelector.getLimit(state),
  page: userSelector.getPage(state),
  count: userSelector.getCount(state),
});

const mapDispatchToProps = (dispatch) => ({
  updateUser: (user) => dispatch(userAction.updateUser(user)),
  deleteUser: (id) => dispatch(userAction.deleteUser(id)),
  setOrder: (order) => dispatch(userAction.setOrder(order)),
  setOrderBy: (orderBy) => dispatch(userAction.setOrderBy(orderBy)),
  setRowsPerPage: (limit) => dispatch(userAction.setLimit(limit)),
  setPage: (page) => dispatch(userAction.setPage(page)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation()(UsersTable));
