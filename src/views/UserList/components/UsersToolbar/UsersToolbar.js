import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Button, Grid, CircularProgress } from '@material-ui/core';
import debounce from 'lodash/debounce';
import { withTranslation } from 'react-i18next';

import { SearchInput, RangeSlider } from '../../../../components';
import UserDetailsModal from '../UserDetailsModal';
import { userAction, userSelector } from '../../../../reducers/users';

const useStyles = makeStyles((theme) => ({
  root: {},
  row: {
    marginTop: theme.spacing(1),
    alignItems: 'center',
    display: 'flex',
  },
  spacer: {
    flexGrow: 1,
  },
  importButton: {
    marginRight: theme.spacing(1),
  },
  searchInput: {
    marginRight: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  spinner: {
    marginRight: theme.spacing(2),
  },
}));

const UsersToolbar = ({
  className,
  createUser,
  maxSalary,
  minSalary,
  setQuery,
  setQuerySalaryRange,
  query,
  uploadUsers,
  uploading,
  t,
  ...rest
}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleSearchUser = debounce(setQuery, 200);

  const handleFileChange = (e) => {
    if (e.target.id === 'contained-button-large-file') {
      uploadUsers(e.target.files[0], { delay: 10000 });
    } else {
      uploadUsers(e.target.files[0]);
    }
    e.target.value = '';
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <div className={classes.row}>
        <span className={classes.spacer} />

        <input
          accept=".csv"
          className={classes.input}
          id="contained-button-file"
          type="file"
          onChange={handleFileChange}
        />
        <input
          accept=".csv"
          className={classes.input}
          id="contained-button-large-file"
          type="file"
          onChange={handleFileChange}
        />
        {uploading ? (
          <CircularProgress className={classes.spinner} />
        ) : (
          <React.Fragment>
            <label htmlFor="contained-button-large-file">
              <Button component="span" className={classes.importButton}>
                {t('Import Large File')}
              </Button>
            </label>
            <label htmlFor="contained-button-file">
              <Button component="span" className={classes.importButton}>
                {t('Import')}
              </Button>
            </label>
          </React.Fragment>
        )}

        <Button color="primary" variant="contained" onClick={handleOpen}>
          {t('Add User')}
        </Button>
        <UserDetailsModal
          title={t('Add User')}
          open={open}
          handleClose={handleClose}
          onSave={(userData) => {
            handleClose();
            createUser(userData);
          }}
        />
      </div>
      <Grid className={classes.row} container spacing={3}>
        <Grid item md={6} xs={12}>
          <SearchInput
            className={classes.searchInput}
            placeholder={t('Search Users')}
            onChange={(e) => handleSearchUser(e.target.value)}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <RangeSlider
            title={t('Salary Range')}
            max={maxSalary}
            min={minSalary}
            onChangeCommitted={(e, values) => setQuerySalaryRange(values)}
          />
        </Grid>
      </Grid>
    </div>
  );
};

UsersToolbar.propTypes = {
  className: PropTypes.string,
  createUser: PropTypes.func,
  maxSalary: PropTypes.number,
  minSalary: PropTypes.number,
  setQuery: PropTypes.func,
  setQuerySalaryRange: PropTypes.func,
  query: PropTypes.string,
  uploadUsers: PropTypes.func,
  uploading: PropTypes.bool,
  t: PropTypes.func,
};

const mapStateToProps = (state) => ({
  maxSalary: userSelector.getMaxSalary(state),
  minSalary: userSelector.getMinSalary(state),
  uploading: userSelector.getUploading(state),
});

const mapDispatchToProps = (dispatch) => ({
  createUser: (userData) => dispatch(userAction.createUser(userData)),
  uploadUsers: (userData, options) =>
    dispatch(userAction.uploadUsers(userData, options)),
  setQuery: (query) => dispatch(userAction.setQuery(query)),
  setQuerySalaryRange: (range) =>
    dispatch(userAction.setQuerySalaryRange(range)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation()(UsersToolbar));
