import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';

import { UsersToolbar, UsersTable } from './components';
import { userAction, userSelector } from '../../reducers/users';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const UserList = ({ users, listUsers }) => {
  const classes = useStyles();
  useEffect(() => {
    listUsers();
  }, [listUsers]);

  return (
    <div className={classes.root}>
      <UsersToolbar />
      <div className={classes.content}>
        <UsersTable users={users} />
      </div>
    </div>
  );
};

UserList.propTypes = {
  users: PropTypes.array.isRequired,
  listUsers: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  users: userSelector.getUsers(state),
});

const mapDispatchToProps = (dispatch) => ({
  listUsers: (options) => dispatch(userAction.listUsers(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
