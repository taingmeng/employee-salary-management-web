import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { ThemeProvider } from '@material-ui/styles';
import validate from 'validate.js';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { I18nextProvider } from 'react-i18next';

import { store, persistor } from './store';
import theme from './theme';
import 'react-perfect-scrollbar/dist/css/styles.css';
import i18n from './i18n';
import validators from './common/validators';
import Routes from './Routes';
import { ConsecutiveSnackbars } from './components';

import './assets/scss/index.scss';

const browserHistory = createBrowserHistory();

validate.validators = {
  ...validate.validators,
  ...validators,
};

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ThemeProvider theme={theme}>
        <I18nextProvider i18n={i18n}>
          <Router history={browserHistory}>
            <Routes />
          </Router>
          <ConsecutiveSnackbars />
        </I18nextProvider>
      </ThemeProvider>
    </PersistGate>
  </Provider>
);

export default App;
