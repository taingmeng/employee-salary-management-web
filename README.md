# Employee Salary Management (React)

Web application to browse employee salaries and perform other CMS operations.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

![alt app](documentation/images/app.png "App")


## Available Scripts

In the project directory, you can run:

- `npm start`: Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

- `npm test`: Launches the test runner in the interactive watch mode.

- `npm run build`: Builds the app for production to the `build` folder.

- `npm run eject`: This is a one-way operation. Once you `eject`, you can’t go back!

- `npm run lint`: Lint entire project using eslint.

- `npm run coverage`: Run tests with coverage report.

## Material UI

- Material UI is used as the UI framework.
- Color palette and typography should be fetched from them.
- ConsecutiveSnackbars is used for display error messages or other info. It get messages from Redux store.

## Redux

- Each Reducer module should consist of:
  - **action**: action creator. Action can be simple action which returns the type and payload. Action can also a series of asynchronous actions which return `dispatch` from `redux-thunk`.
  - **selector**: getter for the states. It should accepts state and other params for querying Redux state. `Reselect` should be used to memoise the result if the query is expensive.
  - **reducer**: hold initial states and reducer function.
  - **index**: export action and selector as name export, and export reducer as default export.
- Redux-persist is used for persisting data in the local storage such as language setting.

## i18n

- Localization is available for English (EN) and Chinese (ZH). Unsupported language will fall back to English.

## Networking

- **Axios** is used for making API calls to server.

## Utils

- **Lodash** is used anywhere possible to avoid unnecessary chunk of codes.
- **Moment** is used for date time conversion and formatting.
